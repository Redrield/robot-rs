git submodule update --init --recursive
dir=$(pwd)
echo "Fixing wpilib compiler args"
cd first-rust-competition/wpilib/HAL/allwpilib/shared
sed -i "s/'-Werror', //g" config.gradle
cd $dir
cp root.makefile.patch first-rust-competition/makefile.patch
cd first-rust-competition
echo "Patching root makefile"
patch -i makefile.patch > /dev/null
rm makefile.patch
cd $dir
cp wpilib.makefile.patch first-rust-competition/wpilib/makefile.patch
cd first-rust-competition/wpilib
patch -i makefile.patch > /dev/null
rm makefile.patch
cd $dir
cp rename.sh first-rust-competition/wpilib/HAL
cp HAL.makefile.patch first-rust-competition/wpilib/HAL/makefile.patch
cd first-rust-competition/wpilib/HAL
echo "Patching HAL makefile"
patch -i makefile.patch > /dev/null
rm makefile.patch

cd $dir
cp build.patch ctre-rs/ctre-sys
cd ctre-rs/ctre-sys
echo "Creating lib search dir"
mkdir lib
echo "Downloading phoenix lib"
wget -q https://raw.githubusercontent.com/CrossTheRoadElec/Phoenix-frc-lib/master/libraries/java/lib/libCTRE_PhoenixCCI.so -O lib/libCTRE_PhoenixCCI.so
echo "Patching ctre-sys build.rs"
patch -i build.patch > /dev/null
rm build.patch
