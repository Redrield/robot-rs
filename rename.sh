for I in ./lib/*; do
    new=$(echo "$I" | sed 's/.so.*/.so/')
    if [ $new != $I ]; then
        mv "$I" "$new"
    fi
done

