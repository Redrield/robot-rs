.PHONY: all

all: prepare wpilib

prepare:
	bash prepare.sh

wpilib:
	cd first-rust-competition; make wpilib
