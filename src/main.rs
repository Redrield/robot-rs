extern crate wpilib;
extern crate ctre;

use std::time::Duration;
use std::thread;

use wpilib::RobotBase;
use wpilib::ds::RobotState;

mod robot;
mod intake;

use robot::Robot;

trait TimedRobot {
    const PERIOD: u64 = 20;
    fn create(base: &RobotBase) -> Self;
    fn teleop_init(&mut self);
    fn teleop_periodic(&mut self);
}

macro_rules! robot_user_program {
    ($robot_struct:ident) => {
        fn main() {
            let robot = RobotBase::new().unwrap();
            let mut inst = <$robot_struct as TimedRobot>::create(&robot);
            RobotBase::start_competition();
            let ds = robot.get_ds_instance();
            let mut current_state = RobotState::Disabled;
            loop {
                let state = ds.read().unwrap().get_state();

                match (current_state, state) {
                    (RobotState::Disabled, RobotState::Teleop) => inst.teleop_init(),
                    (RobotState::Teleop, RobotState::Teleop) => inst.teleop_periodic(),
                    _ => unimplemented!()
                }

                current_state = state;
                thread::sleep(Duration::from_millis(<$robot_struct as TimedRobot>::PERIOD));
            }
        }
    }
}

robot_user_program!(Robot);
