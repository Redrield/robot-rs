use super::TimedRobot;

use wpilib::joystick::*;
use wpilib::RobotBase;

use intake::IntakeSubsystem;

pub struct Robot {
    joystick: Joystick,
    intake: IntakeSubsystem
}

impl TimedRobot for Robot {
    fn create(base: &RobotBase) -> Self {
        let joystick = Joystick::new(&base, 1);

        Robot {
            joystick,
            intake: IntakeSubsystem::new()
        }
    }

    fn teleop_init(&mut self) {

    }

    fn teleop_periodic(&mut self) {
        let forward = self.joystick.get_raw_axis(3).unwrap();
        let backward = self.joystick.get_raw_axis(2).unwrap();

        let axis = forward - backward;

        self.intake.set(axis);
    }
}