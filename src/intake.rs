use ctre::motor_control::*;

pub struct IntakeSubsystem {
    motor: TalonSRX
}

impl IntakeSubsystem {
    pub fn new() -> IntakeSubsystem {
        let left = TalonSRX::new(14);
        let right = TalonSRX::new(21);
        right.follow(&left, FollowerType::PercentOutput);

        IntakeSubsystem {
            motor: left
        }
    }

    pub fn set(&mut self, out: f32) {
        self.motor.set(ControlMode::PercentOutput, out as f64, DemandType::Neutral, 0.0);
    }
}