# Robot-rs

This project is a proof of concept of FRC robot user code written in Rust.

It uses wpilib bindings from [here](https://github.com/Lytigas/first-rust-competition), and CTRE Phoenix bindings from [here](https://github.com/auscompgeek/ctre-rs)

# Building
Prereqs: make, wget, docker

1. Run `make` in the root directory to set up the environment. This will initialize submodules, and configure them in a way that they are ready to use.
2. Run `docker build .` in the root directory to create the docker image
3. Run a container from the created docker image. Mount the root directory as a volume to wherever you please
4. Run `cargo build` inside that container.
5. (Optionally), configure proper team metadata in the root `Cargo.toml` and try to deploy. I have no idea if this actually works


# Questions
## It doesn't work for me, what do I do?

¯\\\_(ツ)\_/¯
